new Vue({
    el:'#vue-app',
    data:{
       todo:[],
       input:""
       
    },
    methods: {
        addTask() {
          var task=this.input;
           if(task){
               this.todo.push(task);
           }
           this.input="";
        },
        deleteTask: function (task) {
        var index = this.todo.indexOf(task);
        this.todo.splice(index, 1);
    },
    remove(){
        this.todo="";
    }
}
})